<?php

/**
 * Implementation of hook_install()
 */
function book_bridge_install() {
  switch ($GLOBALS['db_type']) {
    case 'mysql':
    case 'mysqli':
      db_query("CREATE TABLE {book_bridge} (
        bid int NOT NULL default '0',
        nid int NOT NULL default '0',
        PRIMARY KEY (nid),
        KEY bid (bid)
      ) /*!40100 DEFAULT CHARACTER SET UTF8 */ ");
      break;
    case 'pgsql':
      db_query("CREATE TABLE {book_bridge} (
        bid int NOT NULL default '0',
        nid int NOT NULL default '0',
        PRIMARY KEY (bid)
      )");
      db_query("CREATE INDEX {book_bridge}_bid_idx ON {book_bridge} (bid)");
      break;
  }
  global $base_url;
  if (db_result(db_query("SELECT COUNT(*) FROM {book}"))) {
    drupal_set_message(t('Book Bridge has been installed.  Please visit <a href="@book-bridge">admin/build/book-bridge</a> to index existing book nodes into Book Bridge.', array('@book-bridge' => $base_url .'/admin/build/book-bridge')));
  }
}

/**
 * This code is modeled off the book update function to Drupal 6.
 * Book Bridge is populated by existing book nodes, looking up the root book parent of each.
 */
function book_bridge_update_1() {
  $ret = array();
  if (!isset($_SESSION['book_bridge_update_1'])) {
    // Determine whether there are any existing nodes in the book hierarchy.
    if (db_result(db_query("SELECT COUNT(*) FROM {book}"))) {
 
      $_SESSION['book_bridge_update_1'] = array();
      db_query("DELETE FROM {book_bridge}");
      $result = db_query("SELECT b.* from {book} b INNER JOIN {node} n on b.vid = n.vid WHERE b.parent = 0 ");

      // Collect all books - top-level nodes.
      while ($a = db_fetch_array($result)) {
        $_SESSION['book_bridge_update_1'][] = $a;
      }
      $ret['#finished'] = FALSE;
      return $ret;
    }
    else {
      // No exising nodes in the hierarchy, so do nothing.
      $ret['#finished'] = TRUE;
      return $ret;
    }
  }
  else {
    // Do the next batched part of the update
    $update_count = 100; // Update this many at a time

    while ($update_count && $_SESSION['book_bridge_update_1']) {
      // Get the last node off the stack.
      $book = array_pop($_SESSION['book_bridge_update_1']);

      // Add all of this node's children to the stack
      $result = db_query("SELECT b.* FROM {book} b INNER JOIN {node} n on b.vid = n.vid WHERE b.parent = %d ", $book['nid']);
      while ($a = db_fetch_array($result)) {
        $_SESSION['book_bridge_update_1'][] = $a;
      }

      if ($book['parent']) {
        // If its not a top level page, get its parent's bid.
        $parent = db_fetch_array(db_query("SELECT b.bid FROM {book_bridge} b WHERE b.nid = %d", $book['parent']));
        $book = array_merge($book, $parent);
      }
      else {
        // There is not a parent - this is a new book.
        $book['bid'] = $book['nid'];
      }

      db_query("INSERT INTO {book_bridge} (bid, nid) VALUES (%d, %d)", $book['bid'], $book['nid']);
  
      $update_count--;
    }
    $ret['#finished'] = FALSE;
  }

  if (empty($_SESSION['book_bridge_update_1'])) {
    $ret['#finished'] = TRUE;
    $ret[] = array('success' => TRUE, 'query' => 'Re-indexed existing book pages into Book Bridge.');
    unset($_SESSION['book_bridge_update_1']);
  }

  return $ret;
}

/**
 * Implementation of hook_uninstall().
 */
function book_bridge_uninstall() {
  db_query('DROP TABLE {book_bridge}');
  variable_del('book_bridge_index');
}
