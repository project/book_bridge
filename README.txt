Book Bridge serves to bring some of the functionality of D6 book module to D5.  It should not be necessary in D6.

Specifically Book Bridge keeps track of which book a book node belongs to, enabling such future features as views integration to filter for nodes within a particular book.  It currently includes token integration which can allow for pathauto setups such as BOOKNAME/TITLE.

Be sure to visit admin/build/book-bridge and then run update.php after installing Book Bridge in order to index any existing book nodes.

Book Bridge is being developed by Zivtech for Flat World Knowledge.